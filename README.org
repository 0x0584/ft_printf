* About

=ft_printf= is a project which leads to the AI branch. sounds good! It was based on the [[https://linux.die.net/man/3/printf][printf(3)]] in the man page. The main idea is to take a [[https://alvinalexander.com/programming/printf-format-cheat-sheet][formatting string]] and a set of arguments, process those arguments then print the /formatted result/ to standard output. Also we can print the result to a file descriptor or a string using either =ft_dprintf= or =ft_asprintf= (ft_asprintf allocates memory).

To get into this, we need to handle [[https://en.wikipedia.org/wiki/UTF-8][UTF-8]] in order to print =wchar_t= strings, also in order to handle floating point numbers we need to handle [[http://www.ryanjuckett.com/programming/printing-floating-point-numbers/][IEEE 754]] format. Otherwise, the rest is just conversions from int to ASCII and checking this and setting that.. mostly if-statements.

* Features

** Special

+ taking width and precision via argument as in =%*d= or =%.*s= or even =%*.*f=.
+ reordering arguments using =d$= as in =%2$#08x=, which might be re-usable.
+ Note :: The two should not be mixed, or you the behavior is undefined. Also if =$= is used with one argument, all next arguments must have =$=, otherwise, the behavior is undefined.

** Conversions

+ integer numbers
  + =dDi= as =int=
  + =bBouxX= as =unsigned int= in bases 2, 8, 10, 16
+ floating point numbers
  + =fF= as normal floating point
  + =eE= as scientific exponent
  + =gG= as suitable representation
  + =aA= as hexadecimal representation
+ strings and characters
  + =sS= for ASCII string and UTF-8 strings
  + =rv= for ASCII strings only (reserved and non-printables)
  + =cC= for characters and UTF-8 characters
+ memory addresses =p=

** Length modifiers

+ integer numbers for either signed ot unsigned
    + =hh= as =char=
    + =h= as =short=
    + =l= as =long= (if used with =sc= it's like =SC=. ignored when =fFeEgGaA=)
    + =ll= as =long long=
    + =z= as =size_t=
    + =j= as =intmax_t=
+ floating point numbers
    + =L= as =long double= used with =fFeEgGaA=

** Colors

used like =%{color}= and the colors are te standard POSIX 8 colors, red, yellow, green, blue, cyan, magenta. As well as black & white. Having a suffix =_fg= or =_bg= for foregraound and background. =reset= is a special color, it resets the output to the original color.

* How everything is implemented

Programmers had problems, back in the 70's, to display any form of data in a /well defined format/. It's back then when C programming got its famous format =%=. It very simple, but a bit more spesific. I shall explain my implementation of =ft_printf=, however it will take ages to write every detail down so the following is like brute ideas with some code examples when needed. Althought, the man page of [[https://linux.die.net/man/3/printf][printf(3)]] and the [[https://www.duckduckgo.com][web]] are good company so no worries.

** The format string

The formatting string is divided into parts in a well defined order. Generally speaking it would look something like this: =%{n$}{flags}{field width}{.precision}{length modifier}{conversion specifier}=. Each part controlles a spesific part of the formmated output, obviously, and in some cases one part could dominate another part e.g. =precision= dominates flag ='0'=.

Now, talking about the data structure that would represent what we've said above, it would hold all the information a single formatter. So let's start by defining each part:

- Relative arguments
-


** Relative arguments (=n$=)

#+begin_quote
    Flag =n$= indicates that the current formatter to used based on the given index =n > 1=. However, the behaviour is undefined when mixing =relative arguments= and normal ones i.e. due to the fact that printf is a part of =libc=, but the implmentation is *not*, since this is unspecified implementation detail.
#+end_quote

After that being said, the straitforward approach would do the job; Store all arguments

* Examples

#+BEGIN_SRC c
// (-0010) (+00010)
ft_printf("(% 0*d) (%+*.*d)", 5, -10, 5, 5, 10);

// (001412) (0x0584) (001412) (002604)
ft_printf("(%1$#06d) (%1$#06x) (%1$#06u) (%1$#06o)", 1412);

// (+3) (3.142e+00) (0x1.5bedfa43fe5c9p+1) (  +3.142)
ft_printf("(%2$.f) (%1$05.3e) (%2$a) (%1$8.4g)", 3.1415, 2.7182);

// (     ♥♦♣) (     thi) (    ♥♦♣♠) (    this)
ft_printf("(%2$8.3ls) (%1$8.3s) (%2$8ls) (%1$8s)", "this", L"♥♦♣♠");
#+END_SRC
